# frozen_string_literal: true

require 'bundler'
ENV['BUNDLE_GEMFILE'] ||= Bundler.settings[:gemfile]
require 'bundler/setup'
